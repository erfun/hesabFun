package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"encoding/json"

	"github.com/stretchr/testify/assert"
	"github.com/gin-gonic/gin"
)

func TestHelloWorldRoute(t *testing.T) {
	router := setupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/", nil)
	router.ServeHTTP(w, req)

	//JSON encode our body data
	jsonEncoded, _ := json.Marshal(gin.H{"message":"Hello world!"})

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, string(jsonEncoded), w.Body.String())
}